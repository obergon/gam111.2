using UnityEngine;
using UnityEngine.AI;

// Use physics raycast hit from mouse click to set agent destination
[RequireComponent(typeof(NavMeshAgent))]
public class ClickToMove : MonoBehaviour
{
	private NavMeshAgent navMeshAgent;
	private RaycastHit hitInfo = new RaycastHit();

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && navMeshAgent.enabled && !Input.GetKey(KeyCode.LeftShift))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			// if raycast hit a collider, set the agent's destination to the hit point
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
                navMeshAgent.destination = hitInfo.point;
        }
    }
}
