﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


// mainly handles switching in and out of battle scene
public class OverWorld : MonoBehaviour
{
	public Camera overworldCamera;

	public float worldSize = 64.0f;      // -32 to 32 - used for bounds of enemy random roaming

	public BattleContestants battleContestants;     // SO - set on encounter

	public AudioSource overworldMusic;
	public AudioSource battleMusic;     // fades in on switch to battle scene and then stays playing (silently if switch back to overworld)
	public float musicFadeTime;

	public FadeToBlack fadeToBlack;
	public float musicVolume;

	private bool gameOver = false;


	private void Start()
	{
		overworldMusic.volume = musicVolume;
		overworldMusic.Play();

		InitBattleContestants();
	}

	private void OnEnable()
	{
		BattleEvents.OnActivateOverWorldCamera += OnOverWorldCamera;
		BattleEvents.OnEnterBattle += OnEnterBattle;
		BattleEvents.OnExitBattle += OnExitBattle;
		BattleEvents.OnGameOver += OnGameOver;
	}

	private void OnDisable()
	{
		BattleEvents.OnActivateOverWorldCamera -= OnOverWorldCamera;
		BattleEvents.OnEnterBattle -= OnEnterBattle;
		BattleEvents.OnExitBattle -= OnExitBattle;
		BattleEvents.OnGameOver -= OnGameOver;
	}


	public IEnumerator EnterBattleScene(Player battlePlayer, Enemy battleEnemy)
	{
		FadeInBattleMusic();
		yield return StartCoroutine(fadeToBlack.Fade(true));

		battleContestants.player = battlePlayer;
		battleContestants.playerPosition = battlePlayer.transform.position;
		battleContestants.playerRotation = battlePlayer.transform.rotation;
        battleContestants.player.SetUIText("*");

        battleContestants.enemy = battleEnemy;
		battleContestants.enemyPosition = battleEnemy.transform.position;
		battleContestants.enemyRotation = battleEnemy.transform.rotation;
        battleContestants.enemy.SetUIText("");

        SceneManager.LoadScene("BattleScene", LoadSceneMode.Additive);

		StartCoroutine(fadeToBlack.Fade(false));
		yield return null;
	}


	private void OnEnterBattle()
	{
		if (BattleEvents.OnActivateOverWorldCamera != null)
			BattleEvents.OnActivateOverWorldCamera(false);

		if (BattleEvents.OnOverWorldPaused != null)
			BattleEvents.OnOverWorldPaused(true);
	}


	private void OnOverWorldCamera(bool activate)
	{
		overworldCamera.enabled = activate;
	}


	private void OnExitBattle()
	{
		StartCoroutine(ExitBattleScene());
	}

	// player won battle .. fade out battle scene, put player back in overworld and destroy defeated enemy
	private IEnumerator ExitBattleScene()
	{
		FadeInOverworldMusic();
		yield return StartCoroutine(fadeToBlack.Fade(true));

		Destroy(battleContestants.enemy.gameObject);      // brutal, but this is a prototype!
													
		// put player back where he/she came from
		battleContestants.player.transform.position = battleContestants.playerPosition;
		battleContestants.player.transform.rotation = battleContestants.playerRotation;

		if (BattleEvents.OnActivateOverWorldCamera != null)
			BattleEvents.OnActivateOverWorldCamera(true);
			
		SceneManager.UnloadSceneAsync("BattleScene");

		yield return StartCoroutine(fadeToBlack.Fade(false));

		if (BattleEvents.OnOverWorldPaused != null)
			BattleEvents.OnOverWorldPaused(false);
			
		yield return null;
	}

	private void OnGameOver()
	{
		gameOver = true;
	}


	[ContextMenu("FadeInBattleMusic")]
	public void FadeInBattleMusic()
	{
		battleMusic.Play();
		FadeMusicVolumes(musicVolume, 0);
	}

	[ContextMenu("FadeInOverworldMusic")]
	public void FadeInOverworldMusic()
	{
		FadeMusicVolumes(0, musicVolume);		// keep battle music playing (silently)
	}

	private void FadeMusicVolumes(float low, float high)
	{
		LeanTween.value(low, high, musicFadeTime)
				 .setOnUpdate(x => overworldMusic.volume = x);

		LeanTween.value(high, low, musicFadeTime)
				 .setOnUpdate(x => battleMusic.volume = x);
	}


	private void InitBattleContestants()
	{
		battleContestants.player = null;
		battleContestants.playerPosition = Vector3.zero;
		battleContestants.playerRotation = Quaternion.identity;

		battleContestants.enemy = null;
		battleContestants.enemyPosition = Vector3.zero;
		battleContestants.enemyRotation = Quaternion.identity;
	}
}
