﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


// base class for player and enemies
public abstract class Fighter : MonoBehaviour
{
	public enum State
	{
		Paused,
		Search,
		Chase,
		Engage,
		InBattle,
		Defeated
	}

	public State state = State.Search;
	public FighterData fighterData;                 // stats & skills

	public Text UIText;

	protected OverWorld overWorld;
	protected NavMeshAgent navMeshAgent;   			// when in overworld only
	protected Animator battleAnimator;       		// when in battle only

	protected FighterAnimator fighterAnimator;      // LeanTweens - when in battle only (used simply to face up)

	private const float particleLifetime = 2.0f;    // destroy after this time - could get lifetime from particle sys but this is easier
	private const float sfxVolume = 30f;			// to be heard above music

	private const string faintAnimationTrigger = "Faint";


	public delegate void MoveStartedDelegate();
	public MoveStartedDelegate OnMoveStarted;

	public delegate void MoveCompletedDelegate();
	public MoveCompletedDelegate OnMoveCompleted;

	public delegate void OnDefeatedDelegate();
	public OnDefeatedDelegate OnDefeated;


	protected void Start()
	{
		navMeshAgent = GetComponent<NavMeshAgent>();
		battleAnimator = GetComponent<Animator>();
		fighterAnimator = GetComponent<FighterAnimator>();

		var gameWorldObject = GameObject.Find("OverWorld");
		overWorld = gameWorldObject.GetComponent<OverWorld>();

		Init();
	}


	protected virtual void Init()
	{
		// overrides init any special data here
		state = State.Search;
		battleAnimator.enabled = false;
	}

	private void OnEnable()
	{
		BattleEvents.OnOverWorldPaused += OverWorldPaused;
		BattleEvents.OnBattleStateChanged += OnBattleStateChanged;
		BattleEvents.OnGameOver += OnGameOver;
	}

	private void OnDisable()
	{
		BattleEvents.OnOverWorldPaused -= OverWorldPaused;
		BattleEvents.OnBattleStateChanged -= OnBattleStateChanged;
		BattleEvents.OnGameOver -= OnGameOver;
	}

	private void OnBattleStateChanged(BattleManager.BattleState battleState)
	{
		switch (battleState)
		{
			case BattleManager.BattleState.NotInBattle:
				battleAnimator.enabled = false;
				navMeshAgent.enabled = true;
				break;

			case BattleManager.BattleState.SkillSelection:
				if (state == State.InBattle)
					battleAnimator.enabled = true;

				navMeshAgent.enabled = false;
				break;

			case BattleManager.BattleState.ExecutingMove:
				if (state == State.InBattle)
					battleAnimator.enabled = true;

				navMeshAgent.enabled = false;
				break;

			default:
				break;
		}
	}

	private void OnGameOver()
	{
		Stop();
	}


	// get list of attacking or defending skills only, according to whether fighter is attacking
	// could also use lambda expression ... 
	public List<SkillData> TurnSkills
	{
		get
		{
			List<SkillData> battleSkills = new List<SkillData>();

			foreach (var skill in fighterData.Stats.SkillList)
			{
				if (skill.isAttack == fighterData.isAttacking)
					battleSkills.Add(skill);
			}

			return battleSkills;
		}
	}

	// enemy selects a skill at random after player has selected theirs
	public void RandomSelectSkill()
	{
		var turnSkills = TurnSkills;

		var skillIndex = Random.Range(0, turnSkills.Count);
		fighterData.SelectedSkill = turnSkills[skillIndex];

		if (BattleEvents.OnEnemySkillSelected != null)
			BattleEvents.OnEnemySkillSelected(fighterData.SelectedSkill);
	}

	public virtual void FaceUp()
	{
		// overrides turn to face opponent
	}

	public void ExecuteSelectedSkill()
	{
		if (fighterData.SelectedSkill == null || fighterData.SelectedSkill.AnimatorTrigger == null)
			return;

		TriggerSkillAnimation();        // particle and audio triggered as events during animation

		if (OnMoveStarted != null)
			OnMoveStarted();
	}


	// fire off animation for the selected skill
	protected void TriggerSkillAnimation()
	{
		if (fighterData.SelectedSkill == null || string.IsNullOrEmpty(fighterData.SelectedSkill.AnimatorTrigger))
			return;

		if (battleAnimator != null)
			battleAnimator.SetTrigger(fighterData.SelectedSkill.AnimatorTrigger);
	}

	// animator event
	public void PlayParticles()
	{
		if (fighterData.SelectedSkill != null && fighterData.SelectedSkill.ParticlesPrefab != null)
		{
			var particlesObject = Instantiate(fighterData.SelectedSkill.ParticlesPrefab, transform) as GameObject;
			var particles = particlesObject.GetComponent<ParticleSystem>();

			particles.Play();
			Destroy(particlesObject, particleLifetime);
		}
	}

	// animator event
	public void PlayWhiffAudio()
	{
		if (fighterData.SelectedSkill != null && fighterData.SelectedSkill.WhiffAudio != null)
			AudioSource.PlayClipAtPoint(fighterData.SelectedSkill.WhiffAudio, Vector3.zero, sfxVolume);
	}

	// animator event
	public void PlayHitAudio()
	{
		if (fighterData.SelectedSkill != null && fighterData.SelectedSkill.HitAudio != null)
			AudioSource.PlayClipAtPoint(fighterData.SelectedSkill.HitAudio, Vector3.zero, sfxVolume);
	}

	// animator event
	public void MoveCompleted()
	{
		if (OnMoveCompleted != null)
			OnMoveCompleted(); //fighterData.isAttacking);
	}

	// animator event
	public void OnDefeat() 		// end of faint animation
	{
		if (OnDefeated != null)
			OnDefeated();
	}


	public void Stop()
	{
		if (navMeshAgent.enabled)
		{
			navMeshAgent.isStopped = true;       // stop navigating to destination
			navMeshAgent.ResetPath();
			navMeshAgent.enabled = false;
		}

		if (battleAnimator != null)
			battleAnimator.enabled = false;
	}

	public virtual void TakeDamage(float attackDamage)
	{
		// reduce damage and broadcast event to UI
		if (attackDamage > 0)
			fighterData.Stats.HealthPoints -= attackDamage;

		if (fighterData.Stats.HealthPoints <= 0)
		{
			fighterData.Stats.HealthPoints = 0;

			if (battleAnimator != null)
				battleAnimator.SetTrigger(faintAnimationTrigger);
		}
	}

    public void Exclaim()
    {
        SetUIText("!");

        if (fighterData.exclaimAudio != null)
            AudioSource.PlayClipAtPoint(fighterData.exclaimAudio, Vector3.zero, sfxVolume);
    }

	public void SetUIText(string text)
	{
		UIText.text = text;
	}

	private void OverWorldPaused(bool paused)
	{
		if (paused)
		{
			navMeshAgent.enabled = false;
			if (state != State.InBattle)
				state = State.Paused;
		}
		else
		{
			navMeshAgent.enabled = true;
			state = State.Search;
		}
	}
}

