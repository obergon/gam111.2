﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FighterAnimator : MonoBehaviour
{
	private Fighter fighter;

	//public float windUpDistance = 0.33f;        // on z axis
	//public float windUpTime = 0.25f;            // seconds

	//public float advanceDistance = 1.0f;        // on z axis
	//public float advanceTime = 0.2f;            // seconds

	public float faceUpRotation = 90.0f;
	public float faceUpTime = 0.5f;            // seconds

	//public float kickRotation = 90.0f;
	//public float punchRotation = 60.0f;

	//public Vector3 idleScale = new Vector3(1.9f, 1.55f, 1.8f);
	//public float idleTime = 3.0f;            // seconds

	//public float strikeTime = 0.25f;            // seconds


	private void Awake()
	{
		fighter = GetComponent<Fighter>();		// Fighter script must be present on object
	}


	public void FaceUp(bool isPlayer)
	{
		if (fighter == null)
			return;

		LeanTween.rotateY(fighter.gameObject, isPlayer ? faceUpRotation : -faceUpRotation, faceUpTime)
				 .setEase(LeanTweenType.easeInCubic);
	}

	//public void Advance()
	//{
	//	if (fighter == null)
	//		return;

	//	// wind-up
	//	LeanTween.moveX(fighter.gameObject, -windUpDistance, windUpTime)
	//			 .setEase(LeanTweenType.easeInCubic)
	//			 .setLoopPingPong(1)
	//			 .setOnComplete(() =>
	//			 {
	//				// advance
	//				LeanTween.moveX(fighter.gameObject, advanceDistance, advanceTime)
	//				  		.setLoopPingPong(1)
	//				  		.setEase(LeanTweenType.easeInCubic)
	//				  		.setOnComplete(() =>
	//						{
	//							fighter.PlayHitAudio();
	//							fighter.PlayParticles();
	//		   				});
	//		 	});
	//}

	//public void Punch()
	//{
	//	if (fighter == null)
	//		return;

	//	LeanTween.rotateY(fighter.gameObject, punchRotation, strikeTime)
	//			 .setLoopPingPong(1)
	//			 .setEase(LeanTweenType.easeInCubic)
	//			 .setOnComplete(() => { fighter.MoveCompleted(); });
	//}

	//public void Kick()
	//{
	//	if (fighter == null)
	//		return;

	//	LeanTween.rotateX(fighter.gameObject, -kickRotation, strikeTime)
	//			 .setLoopPingPong(1)
	//			 .setEase(LeanTweenType.easeInCubic)
	//			 .setOnComplete(() => { fighter.MoveCompleted(); });
	//}

	//public void Idle()
	//{
	//	if (fighter == null)
	//		return;

	//	LeanTween.scale(fighter.gameObject, idleScale, idleTime)
	//	         .setLoopPingPong()
	//			 .setEase(LeanTweenType.easeInSine);
	//}
}
