﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// manages 2 contestants in battle and broadcasts to UI
public class BattleManager : MonoBehaviour
{
	public enum BattleState
	{
		NotInBattle,                // not in battle mode
		SkillSelection,             // waiting for play to select skill
		ExecutingMove,              // attack / defence moves being executed
		GameOver                    // player lost
	};

	private BattleState battleState = BattleState.NotInBattle;

	public Vector3 playerPosition;
	public Quaternion playerRotation = Quaternion.identity;
	public Vector3 enemyPosition;
	public Quaternion enemyRotation = Quaternion.identity;

	public BattleContestants contestants;     // SO - setup in overworld when enemy engages player

	public float feedbackTime = 1.0f;         	// time for feedback to show before fading	
	public float victoryFeedbackTime = 3.0f;    // time for feedback to show before fading	

	private Fighter attacker;
	private Fighter defender;

	private bool attackCompleted = false;
	private bool defenceCompleted = false;

	private float turnAttackDamage = 0f;      // net outcome of attack skill vs. defence skill


	private bool PlayerIsAttacker
	{
		get { return contestants.player.GetInstanceID() == attacker.GetInstanceID(); }
	}


	private void OnEnable()
	{
		InitBattle();

		// broadcast event for 2 fighters in the battle
		if (BattleEvents.OnSetContestants != null)
			BattleEvents.OnSetContestants(contestants);        // eg. set up stats in UI

		BattleEvents.OnPlayerSkillSelected += OnPlayerSkillSelected;
	}

	private void OnDisable()
	{
		BattleEvents.OnPlayerSkillSelected -= OnPlayerSkillSelected;

		attacker.OnMoveStarted -= AttackStarted;
		attacker.OnMoveCompleted -= AttackCompleted;
		attacker.OnDefeated -= AttackerDefeated;

		defender.OnMoveStarted -= DefenceStarted;
		defender.OnMoveCompleted -= DefenceCompleted;
		defender.OnDefeated -= DefenderDefeated;
	}

	private void InitBattle()
	{
		if (BattleEvents.OnEnterBattle != null)
			BattleEvents.OnEnterBattle();

		if (contestants.player != null)
		{
			var playerTransform = contestants.player.transform;
			var playerData = contestants.player.fighterData;

			playerTransform.position = playerPosition;
			playerTransform.rotation = playerRotation;

			playerData.Stats.HealthPoints = playerData.MaxHealth;

			contestants.player.FaceUp();          // turn to face opponent
		}

		if (contestants.enemy != null)
		{
			var enemyTransform = contestants.enemy.transform;
			var enemyData = contestants.enemy.fighterData;

			enemyTransform.position = enemyPosition;
			enemyTransform.rotation = enemyRotation;

			enemyData.Stats.HealthPoints = enemyData.MaxHealth;

			contestants.enemy.FaceUp();          // turn to face opponent
		}

		ChangeTurn();     // first turn, so fastest fighter attacks first
	}


	private void SetBattleState(BattleState state)
	{
		battleState = state;

		if (BattleEvents.OnBattleStateChanged != null)
			BattleEvents.OnBattleStateChanged(state);
	}


	public void TakeTurn()
	{
		SetBattleState(BattleState.ExecutingMove);      // broadcast event

		turnAttackDamage = CalculateAttackDamage();     // net effect of fighter stats and skills selected
		ExecuteMoves();                             // attack + defence - change turn after attack has completed
	}

	// fighter with fastest speed attacks first
	private void ChangeTurn()
	{
		if (contestants.player == null || contestants.enemy == null)
			return;

		attackCompleted = false;
		defenceCompleted = false;

		if (attacker == null || defender == null)       // must be first turn
		{
			if (contestants.player.fighterData.Stats.Speed >= contestants.enemy.fighterData.Stats.Speed)
			{
				attacker = contestants.player;
				defender = contestants.enemy;
			}
			else
			{
				attacker = contestants.enemy;
				defender = contestants.player;
			}
		}
		else
		{
			var prevAttacker = attacker;
			attacker = defender;
			defender = prevAttacker;
		}

		attacker.OnMoveStarted += AttackStarted;
		attacker.OnMoveCompleted += AttackCompleted;
		attacker.OnDefeated += AttackerDefeated;

		defender.OnMoveStarted += DefenceStarted;
		defender.OnMoveCompleted += DefenceCompleted;
		defender.OnDefeated += DefenderDefeated;

		attacker.fighterData.isAttacking = true;
		defender.fighterData.isAttacking = false;
		attacker.fighterData.SelectedSkill = null;
		defender.fighterData.SelectedSkill = null;

		SetBattleState(BattleState.SkillSelection);         // broadcast event

		if (BattleEvents.OnTurnChanged != null)
			BattleEvents.OnTurnChanged(attacker, defender, PlayerIsAttacker);
	}


	private void OnPlayerSkillSelected(SkillData skill)
	{
		if (battleState != BattleState.SkillSelection)
			return;

		contestants.player.fighterData.SelectedSkill = skill;

		// once player has selected their skill, enemy makes a random selection
		contestants.enemy.RandomSelectSkill();
		TakeTurn();     // attacker strikes and defender's HP is reduced (if not a miss)
	}

	// algorithm to determine damage caused by attack
	private float CalculateAttackDamage()
	{
		float attackDamage = 0f;

		if (attacker == null || defender == null)
			return attackDamage;

		// damage inflicted by attack is mitigated by defending move
		var damageOutcome = attacker.fighterData.SelectedSkill.Damage - defender.fighterData.SelectedSkill.Damage;

		// TODO: figure out attackDamage somehow from stats!
		// this is a quick & dirty hack...
		var speedOutcome = attacker.fighterData.Stats.Speed /
								  ((defender.fighterData.Stats.Speed > 0) ? defender.fighterData.Stats.Speed : attacker.fighterData.Stats.Speed);

		var attackOutcome = attacker.fighterData.Stats.Attack /
								((defender.fighterData.Stats.Defence > 0) ? defender.fighterData.Stats.Defence : attacker.fighterData.Stats.Attack);

		var accuracyOutcome = attacker.fighterData.Stats.Accuracy /
								  ((defender.fighterData.Stats.Evade > 0) ? defender.fighterData.Stats.Evade : attacker.fighterData.Stats.Accuracy);

		attackDamage = damageOutcome * speedOutcome * attackOutcome * accuracyOutcome;
		return attackDamage;
	}


	private void ExecuteMoves()
	{
		attacker.ExecuteSelectedSkill();
		defender.ExecuteSelectedSkill();
	}


	// event listener
	private void AttackStarted()
	{
		if (BattleEvents.OnFeedback != null)
			BattleEvents.OnFeedback("", feedbackTime);
	}

	// event listener
	// attacker's move dictates change of turn
	private void AttackCompleted()
	{
		if (turnAttackDamage > 0)
		{
			defender.TakeDamage(turnAttackDamage);          // virtual

			//if (BattleEvents.OnFeedback != null)
				//BattleEvents.OnFeedback("HIT!", feedbackTime);
		}
		else
		{
			if (BattleEvents.OnFeedback != null)
				BattleEvents.OnFeedback("MISS", feedbackTime);
		}

		attacker.OnMoveStarted -= AttackStarted;
		attacker.OnMoveCompleted -= AttackCompleted;
		attacker.OnDefeated -= AttackerDefeated;

		attackCompleted = true;

		if (defenceCompleted)
			ChangeTurn();
	}

	// event listener
	private void DefenceStarted()
	{
		if (BattleEvents.OnFeedback != null)
			BattleEvents.OnFeedback("", feedbackTime);
	}

	// event listener
	private void DefenceCompleted()
	{
		defender.OnMoveStarted -= DefenceStarted;
		defender.OnMoveCompleted -= DefenceCompleted;
		defender.OnDefeated -= DefenderDefeated;

		defenceCompleted = true;

		if (attackCompleted)
			ChangeTurn();
	}

	// event listener
	// end of faint animation
	private void AttackerDefeated()
	{
		if (PlayerIsAttacker) 
			GameOver();
		else
			Victory();              // return to overworld
	}

	// event listener
	// end of faint animation
	private void DefenderDefeated()
	{
		if (!PlayerIsAttacker)
			GameOver();
		else
			Victory();              // return to overworld
	}

	private void GameOver()
	{
		SetBattleState(BattleState.GameOver);

		if (BattleEvents.OnGameOver != null)
			BattleEvents.OnGameOver();            // stop all characters in overworld... they prolly already are

		if (BattleEvents.OnFeedback != null)
			BattleEvents.OnFeedback("SADLY, YOU FAINTED...", 30f);
	}

	private void Victory()
	{
		if (BattleEvents.OnFeedback != null)
			BattleEvents.OnFeedback("VICTORY IS YOURS!", victoryFeedbackTime);

		if (BattleEvents.OnExitBattle != null)
			BattleEvents.OnExitBattle();        // return to overworld
	}
}
