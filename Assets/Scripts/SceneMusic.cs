﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SceneMusic : MonoBehaviour
{
	public AudioSource overworldMusic; 		// plays on awake
	public AudioSource battleMusic;

	public float fadeTime = 1f;

    [ContextMenu("FadeInBattleMusic")]
    public void FadeInBattleMusic()
    {
		battleMusic.Play();
		TweenVolumes(1, 0);
    }

    [ContextMenu("FadeInOverworldMusic")]
    public void FadeInOverworldMusic()
    {
        TweenVolumes(0, 1);
    }

    private void TweenVolumes(float low, float high)
    {
        LeanTween.value(low, high, fadeTime)
                 .setOnUpdate(x => overworldMusic.volume = x);

        LeanTween.value(high, low, fadeTime)
                 .setOnUpdate(x => battleMusic.volume = x);
    }
}
