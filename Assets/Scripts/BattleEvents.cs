﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// as the name suggests...
public static class BattleEvents
{
	public delegate void EnterBattleDelegate();
	public static EnterBattleDelegate OnEnterBattle;

	public delegate void ExitBattleDelegate();
	public static ExitBattleDelegate OnExitBattle;

	public delegate void OverworldCameraDelegate(bool activate);
	public static OverworldCameraDelegate OnActivateOverWorldCamera;

	public delegate void BattleStateChangedDelegate(BattleManager.BattleState battleState);
	public static BattleStateChangedDelegate OnBattleStateChanged;

	public delegate void SetFightersDelegate(BattleContestants contestants);
	public static SetFightersDelegate OnSetContestants;

	public delegate void PlayerSkillSelectedDelegate(SkillData skill);
	public static PlayerSkillSelectedDelegate OnPlayerSkillSelected;

	public delegate void EnemySkillSelectedDelegate(SkillData skill);
	public static EnemySkillSelectedDelegate OnEnemySkillSelected;

	public delegate void TurnChangedDelegate(Fighter attacker, Fighter defender, bool playerIsAttacker);
	public static TurnChangedDelegate OnTurnChanged;

	public delegate void OverworldPausedDelegate(bool paused);
	public static OverworldPausedDelegate OnOverWorldPaused;

	public delegate void PlayerHealthChangedDelegate(float newHealth);
	public static PlayerHealthChangedDelegate OnPlayerHealthChanged;

	public delegate void EnemyHealthChangedDelegate(float newHealth);
	public static EnemyHealthChangedDelegate OnEnemyHealthChanged;

	public delegate void FeedbackDelegate(string feedback, float fadeAfterTime);
	public static FeedbackDelegate OnFeedback;

	public delegate void GameOverDelegate();
	public static GameOverDelegate OnGameOver;
}
