﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// UI for battle scene
public class UIController : MonoBehaviour
{
	public FighterUI PlayerUI;
	public FighterUI EnemyUI;

	public Text Feedback;

	public Text CurrentTurn;		// name of fighter whose turn it is to attack
	public Text BattleState;

	public AudioClip whiffAudio;
	public AudioClip lightHitAudio;
	public AudioClip heavyHitAudio;

	private bool canSelectSkill = false;

	private const float sfxVolume = 20f;            // to be heard above music
	private const float feedbackFadeInTime = 0.5f;
	private const float feedbackFadeOutTime = 2.0f;

	private Animator animator;


	private void Start()
	{
		animator = GetComponent<Animator>();
		animator.SetTrigger("StatsEnter");
	}

	private void OnEnable()
	{
		BattleEvents.OnBattleStateChanged += OnBattleStateChanged;
		BattleEvents.OnSetContestants += OnSetContestants;
		BattleEvents.OnPlayerSkillSelected += OnPlayerSkillSelected;
		BattleEvents.OnEnemySkillSelected += OnEnemySkillSelected;
		BattleEvents.OnPlayerHealthChanged += OnPlayerHealthChanged;
		BattleEvents.OnEnemyHealthChanged += OnEnemyHealthChanged;
		BattleEvents.OnTurnChanged += OnTurnChanged;
		BattleEvents.OnFeedback += OnFeedback;
	}

	private void OnDisable()
	{
		BattleEvents.OnBattleStateChanged -= OnBattleStateChanged;
		BattleEvents.OnSetContestants -= OnSetContestants;
		BattleEvents.OnPlayerSkillSelected -= OnPlayerSkillSelected;
		BattleEvents.OnEnemySkillSelected -= OnEnemySkillSelected;
		BattleEvents.OnPlayerHealthChanged -= OnPlayerHealthChanged;
		BattleEvents.OnEnemyHealthChanged -= OnEnemyHealthChanged;
		BattleEvents.OnTurnChanged -= OnTurnChanged;
		BattleEvents.OnFeedback -= OnFeedback;
	}


	private void OnPlayerSkillSelected(SkillData skill)
	{
		if (!canSelectSkill)
			return;
			
		PlayerUI.SelectSkill(skill);
	}

	private void OnEnemySkillSelected(SkillData skill)
	{
		if (!canSelectSkill)
			return;

		EnemyUI.SelectSkill(skill);
	}


	private void OnPlayerHealthChanged(float newHealth)
	{
		PlayerUI.UpdateHealth(newHealth);
	}

	private void OnEnemyHealthChanged(float newHealth)
	{
		EnemyUI.UpdateHealth(newHealth);
	}


	private void OnTurnChanged(Fighter attacker, Fighter defender, bool playerIsAttacker)
	{
		CurrentTurn.text = attacker.fighterData.FighterName.ToUpper() + " TO ATTACK!";

		// reset skill selection for new turn
		PlayerUI.DeselectSkill();
		EnemyUI.DeselectSkill();

		var playerSkills = playerIsAttacker ? attacker.TurnSkills : defender.TurnSkills;
		var enemySkills = playerIsAttacker ? defender.TurnSkills : attacker.TurnSkills;

		// repopulate skills for attack / defence
		PlayerUI.PopulateTurnSkillButtons(playerSkills, true);
		EnemyUI.PopulateTurnSkillButtons(enemySkills, false);
	}


    private void OnSetContestants(BattleContestants contestants)
	{
		if (contestants.player == null || contestants.enemy == null)
			return;

		PlayerUI.SetFighter(contestants.player, true);
		EnemyUI.SetFighter(contestants.enemy, false);
	}

	private void OnBattleStateChanged(BattleManager.BattleState battleState)
	{
		switch (battleState)
		{
			case BattleManager.BattleState.NotInBattle:
				BattleState.text = "...";
				canSelectSkill = false;
				break;

			case BattleManager.BattleState.SkillSelection:
				BattleState.text = "CHOOSE YOUR MOVE!";
				canSelectSkill = true;
				break;

			case BattleManager.BattleState.ExecutingMove:
				BattleState.text = "IN BATTLE";
				canSelectSkill = false;
				break;

			case BattleManager.BattleState.GameOver:
				BattleState.text = "GAME OVER";
				canSelectSkill = false;
				break;

			default:
				BattleState.text = "UNKNOWN STATE";
				canSelectSkill = false;
				break;
		}
	}


	private void OnFeedback(string feedback, float fadeAfterTime)
	{
		LeanTween.textAlpha(Feedback.rectTransform, 100f, feedbackFadeInTime);

		Feedback.text = feedback;

		LeanTween.textAlpha(Feedback.rectTransform, 0f, feedbackFadeOutTime)
				 .setEase(LeanTweenType.easeOutCirc)
				 .setDelay(fadeAfterTime);
		         //.setOnComplete(() => { Feedback.text = ""; });
	}


	// animator event
	public void PlayWhiffAudio()
	{
		if (whiffAudio != null)
			AudioSource.PlayClipAtPoint(whiffAudio, Vector3.zero, sfxVolume);
	}

	// animator event
	public void PlayLightHitAudio()
	{
		if (lightHitAudio != null)
			AudioSource.PlayClipAtPoint(lightHitAudio, Vector3.zero, sfxVolume);
	}

	// animator event
	public void PlayHeavyHitAudio()
	{
		if (heavyHitAudio != null)
			AudioSource.PlayClipAtPoint(heavyHitAudio, Vector3.zero, sfxVolume);
	}
}
