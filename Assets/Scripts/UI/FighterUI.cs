﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// UI for each fighter in battle scene
public class FighterUI : MonoBehaviour
{
	public Text FighterName;
	public Slider HP;
	public Slider Speed;
	public Slider Attack;
	public Slider Defense;
	public Slider Accuracy;
	public Slider Evade;

	public Text Feedback;

	public LayoutGroup SkillsPanel;
	public GameObject SkillButtonPrefab;
	public float HealthUpdateTime = 1.0f;       // tween time to update HP slider after attack

	private FighterData fighterData;


	public void SetFighter(Fighter fighter, bool interactable)
	{
		fighterData = fighter.fighterData;

		FighterName.text = fighterData.FighterName.ToUpper();

		HP.value =  fighterData.Stats.HealthPoints / fighterData.MaxHealth;
		Speed.value = fighterData.Stats.Speed / 100f;
		Attack.value = fighterData.Stats.Attack / 100f;
		Defense.value = fighterData.Stats.Defence / 100f;
		Accuracy.value = fighterData.Stats.Accuracy / 100f;
		Evade.value = fighterData.Stats.Evade / 100f;

		// populate only with attacking / defending skills, according to fighters turn
		PopulateTurnSkillButtons(fighter.TurnSkills, interactable);
	}

	public void SelectSkill(SkillData selectedSkill)
	{
		foreach (Transform child in SkillsPanel.transform)
		{
			var skillButton = child.GetComponent<SkillButton>();
			skillButton.SetSelected(skillButton.skillName.text == selectedSkill.SkillName);
		}
	}

	public void DeselectSkill()
	{
		foreach (Transform child in SkillsPanel.transform)
		{
			var skillButton = child.GetComponent<SkillButton>();
			skillButton.SetSelected(false);
		}
	}

	public void PopulateTurnSkillButtons(List<SkillData> turnSkills, bool interactable)
	{
		ClearSkillButtons();

		foreach (var skill in turnSkills)
		{
			var buttonObject = Instantiate(SkillButtonPrefab, SkillsPanel.transform) as GameObject;
			var skillButton = buttonObject.GetComponent<SkillButton>();
			skillButton.SetSkillData(skill, interactable);
		}
	}

	private void ClearSkillButtons()
	{
		foreach (Transform child in SkillsPanel.transform)
		{
			var skillButton = child.GetComponent<SkillButton>();
			skillButton.RemoveListeners();

			Destroy(child.gameObject);
		}
	}

	public void UpdateHealth(float newHealth)
	{
		var to = newHealth / fighterData.MaxHealth;

		LeanTween.value(HP.value, to, HealthUpdateTime)
				 .setEase(LeanTweenType.easeInCubic)
				 .setOnUpdate(x => HP.value = x);
	}
}



