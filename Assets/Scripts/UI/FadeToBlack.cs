﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour
{
	public Image blackOut;
	public float fadeTime = 1.5f;

	public IEnumerator Fade(bool toBlack)
	{
		blackOut.color = Color.clear;
		gameObject.SetActive(true);

		float t = 0;
		var startColour = toBlack ? Color.clear : Color.black;
		var targetColour = toBlack ? Color.black : Color.clear;

		while (t < 1.0f)
		{
			t += Time.deltaTime * (Time.timeScale / fadeTime);
			blackOut.color = Color.Lerp(startColour, targetColour, t);
			yield return null;
		}

		if (!toBlack)
			gameObject.SetActive(false);

		yield return null;
	}
}
