﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour
{
	public Image background;
	public Text skillType;
	public Text skillName;
	public Text skillDamage;
	public Outline selectedBorder;
	public Image selectedTick;

	private Button skillButton;
	private SkillData skillData;


	private void OnEnable()
	{
		skillButton = GetComponent<Button>();
	}

	private void OnDisable()
	{
		skillButton.onClick.RemoveAllListeners();
	}

	public void SetSkillData(SkillData data, bool interactable)
	{
		skillData = data;

		background.color = data.ButtonColour;

		skillType.text = data.Type.ToString();
		skillName.text = data.SkillName;
		skillDamage.text = data.Damage.ToString();

		skillButton.interactable = interactable;
		skillButton.onClick.RemoveAllListeners();

		if (interactable)
		{
			skillButton.onClick.AddListener(() =>
			{
				if (BattleEvents.OnPlayerSkillSelected != null)
					BattleEvents.OnPlayerSkillSelected(skillData);
			});
		}
	}

	public void SetSelected(bool isSelected)
	{
		selectedBorder.enabled = isSelected;
		//selectedTick.enabled = isSelected;
	}

	public void RemoveListeners()
	{
		skillButton.onClick.RemoveAllListeners();
	}
}
