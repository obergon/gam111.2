﻿

// abstract class that implements ICatchable interface
// also inherits from Monobehaviour so that GetComponent can be used to access it
public abstract class CatchableFighter : Fighter, ICatchable
{
	public abstract bool HasBeenCaught { get; }
	public abstract void StartBattle(Enemy enemy);      // on capture
}
