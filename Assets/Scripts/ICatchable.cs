﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ICatchable
{
	bool HasBeenCaught { get; }
	void StartBattle(Enemy enemy);
}
