﻿using System;
using UnityEngine;

// specialised fighter with click to move ability
// that can be caught and then start battle with captor
public class Player : CatchableFighter
{
	private RaycastHit hitInfo = new RaycastHit();
	private Enemy caughtByEnemy;

	public override bool HasBeenCaught { get { return caughtByEnemy != null; } }


	protected override void Init()
	{
		base.Init();
		SetUIText("*");
	}

	private void Update()
	{
		if (state != State.Search)
			return;

		// click to move
		if (Input.GetMouseButtonDown(0) && navMeshAgent.enabled && Camera.main.enabled)
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			// if raycast hit a collider, set the nav agent's destination to the hit point
			if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
				navMeshAgent.destination = hitInfo.point;
		}
	}

	public override void StartBattle(Enemy enemy)
	{
		if (HasBeenCaught)
			return;

		caughtByEnemy = enemy;

		state = State.InBattle;
		caughtByEnemy.state = State.InBattle;

        Stop();
		caughtByEnemy.Stop();

		StartCoroutine(overWorld.EnterBattleScene(this, caughtByEnemy));
		caughtByEnemy = null;
	}

	public override void FaceUp()
	{
		if (fighterAnimator != null)
			fighterAnimator.FaceUp(true);
	}

	public override void TakeDamage(float attackDamage)
	{
		// reduce damage
		base.TakeDamage(attackDamage);

		// broadcast event to UI
		if (BattleEvents.OnPlayerHealthChanged != null)
			BattleEvents.OnPlayerHealthChanged(fighterData.Stats.HealthPoints);
	}

	// show line of vision gizmo
	// TODO: testing / debugging only!
	//private void OnDrawGizmos()
	//{
	//	Gizmos.color = Color.green;
	//	Gizmos.DrawRay(transform.position, transform.forward * 5f);
	//}
}


