﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class FighterData : ScriptableObject
{
	public string FighterName;
	public FighterStats Stats;
	public float MaxHealth = 1000f;
	public bool isAttacking;
    public AudioClip exclaimAudio;

	public SkillData SelectedSkill;
}

[System.Serializable]
public struct FighterStats
{
	public float HealthPoints;		// starts at MaxHealth
	public float Speed;             // higher speed fighter makes first move before lower speed (percentage)

	public float Attack;			// determines strength of attacks (percentage)
	public float Defence;           // determines resilience to attacks (percentage)
	public float Accuracy;          // higher accuracy = moves more likely to hit (percentage)
	public float Evade;             // higher evasiveness = opponent's moves more likely to miss (percentage)

	public List<SkillData> SkillList;
}
