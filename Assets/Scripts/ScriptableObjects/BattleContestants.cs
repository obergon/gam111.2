﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// SO to pass battle info from overworld to battlescene
// and record player and enemy positions
[CreateAssetMenu()]
public class BattleContestants : ScriptableObject
{
	public Player player;
	public Vector3 playerPosition;     		// in overworld
	public Quaternion playerRotation;      	// in overworld

	public Enemy enemy;
	public Vector3 enemyPosition;       	// in overworld
	public Quaternion enemyRotation;       	// in overworld
}
