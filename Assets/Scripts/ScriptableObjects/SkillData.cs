﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class SkillData : ScriptableObject
{
	public enum SkillType
	{
		None,
		Bounce,
		ForwardRoll,
		BackFlip,
		DodgeLeft,
		DodgeRight,
		Block,
		CounterAttack,
		PunchLeft,
		PunchRight,
		PunchLeftRight,
		FlyingSpinKick,
		HeadCharge,
		Shove
	}

    public bool isAttack;          		// else defence
	public string SkillName;			// shown on button
	public SkillType Type;
	public float Damage; 				// negative value to counter/cancel out attack damage

	public GameObject ParticlesPrefab;
	public AudioClip WhiffAudio;
	public AudioClip HitAudio;

	public string AnimatorTrigger;

	public Color ButtonColour;
}
