﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


// not used in this project - see BattleEvents class
public class EventManager : MonoBehaviour
{
	public enum EventName
	{
		InBattle,
		InOverWorld,
	};


	private static Dictionary<EventName, UnityEvent> eventDictionary;

	// singleton initialisation
	private static EventManager eventManager;
	public static Dictionary<EventName, UnityEvent> Instance
	{
		get
		{
			if (eventManager == null)
				eventManager = FindObjectOfType<EventManager>();

			if (eventManager == null)
			{
				Debug.LogError("An EventManager script must exist on a GameObject in the scene!");
			}
			else
			{
				eventManager.InitDictionary();
			}

			return eventDictionary;
		}
	}

	private void InitDictionary()
	{
		if (eventDictionary == null)
			eventDictionary = new Dictionary<EventName, UnityEvent>();
	}


	public static void StartListening(EventName name, UnityAction listener)
	{
		UnityEvent foundEvent = null;

		if (eventDictionary.TryGetValue(name, out foundEvent))
		{
			foundEvent.AddListener(listener);
		}
		else 		// not yet in dictionary, so add it!
		{
			foundEvent = new UnityEvent();
			foundEvent.AddListener(listener);

			eventDictionary.Add(name, foundEvent);
		}
	}

	public static void StopListening(EventName name, UnityAction listener)
	{
		UnityEvent foundEvent = null;

		if (eventDictionary.TryGetValue(name, out foundEvent))
		{
			foundEvent.RemoveListener(listener);
		}
	}

	public static void TriggerEvent(EventName name)
	{
		UnityEvent foundEvent = null;

		if (eventDictionary.TryGetValue(name, out foundEvent))
		{
			foundEvent.Invoke();
		}
	}
}


