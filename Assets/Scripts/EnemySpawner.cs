﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// ended up not using this class in the end...
public class EnemySpawner : MonoBehaviour
{
	public List<GameObject> EnemyPrefabs = new List<GameObject>();

	public Enemy SpawnRandomEnemy(Vector3 position)
	{
		var index = Random.Range(0, EnemyPrefabs.Count);

		var enemyObject = Instantiate(EnemyPrefabs[index]) as GameObject;
		enemyObject.transform.position = position;

		return enemyObject.GetComponent<Enemy>();
	}
}
