﻿using System.Collections.Generic;
using UnityEngine;


// specialised fighter that searches for player
// implemented as a state machine
public class Enemy : Fighter
{
	public float maxVisionDist = 5;
    public float minDistToExclaim = 1;

	public bool randomRoam = false;
	public List<Vector3> patrolPath = new List<Vector3>();		// points that constitute the patrol path
	private int currentPath = 0;                                // index into patrolPath list

	private CatchableFighter chatchableTarget;       // implements ICatchable interface - set when discovered (raycast)


	protected override void Init()
	{
		base.Init();

		currentPath = 0;

		if (patrolPath.Count > 0)
			transform.position = patrolPath[currentPath];       // ie. patrol start position

		SetNextPatrolDestination();
	}

    void Update()
    {
        switch (state)
        {
			case State.Search:
				SearchForCatchable();	// chase when 'seen'
                break;

            case State.Chase:
				ChaseCatchable();		// engage (start battle) when close enough
                break;

            case State.Engage:
				break;

			case State.InBattle:
				break;

			case State.Defeated:
                break;

            default:
                break;
        }
    }


	private void SearchForCatchable()
	{
		RaycastHit hit = new RaycastHit();

		if (Physics.Raycast(transform.position, transform.forward, out hit, maxVisionDist))
		{
			// Player implements ICatchable interface via abstract Catchable base class component
			var catchablePlayer = hit.transform.GetComponent<CatchableFighter>();

			if (catchablePlayer != null && !catchablePlayer.HasBeenCaught)        // don't chase if not catchable!
			{
				chatchableTarget = catchablePlayer;
				state = State.Chase;

                Exclaim();
                chatchableTarget.Exclaim();
            }
		}

		// if reached destination, set next destination in patrolPath
		if (state != State.Chase && navMeshAgent.enabled)
		{
			if (!navMeshAgent.pathPending)		// true if still computing nav path
			{
					if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
				{
					SetNextPatrolDestination();
				}
			}
		}
	}

	private void SetNextPatrolDestination()
	{
		if (!navMeshAgent.enabled)
			return;

		if (randomRoam || patrolPath.Count == 0)
		{
			var randomX = UnityEngine.Random.Range(-overWorld.worldSize / 2, overWorld.worldSize / 2);
			var randomZ = UnityEngine.Random.Range(-overWorld.worldSize / 2, overWorld.worldSize / 2);

			navMeshAgent.destination = new Vector3(randomX, transform.position.y, randomZ);
		}
		else
		{
			currentPath++;

			if (currentPath >= patrolPath.Count)
				currentPath = 0;        // return to start

			navMeshAgent.destination = patrolPath[currentPath];
		}
	}

	private void ChaseCatchable()
    {
		if (!navMeshAgent.enabled)
			return;

		navMeshAgent.destination = chatchableTarget.transform.position;

		var distanceToTarget = Vector3.Distance(transform.position, chatchableTarget.transform.position);

		// once the chased (catchable) target is close enough, start a battle!
		if (distanceToTarget < minDistToExclaim)
        {
			state = State.Engage;
			EngageTarget();
		}
	}

	private void EngageTarget()
	{
		if (chatchableTarget != null && !chatchableTarget.HasBeenCaught)
		{
			state = State.InBattle;
			chatchableTarget.StartBattle(this);   // player stops moving and battle sequence commences
		}
	}

	public override void FaceUp()
	{
		if (fighterAnimator != null)
			fighterAnimator.FaceUp(false);
	}

	public override void TakeDamage(float attackDamage)
	{
		// reduce damage
		base.TakeDamage(attackDamage);

		// broadcast event to UI
		if (BattleEvents.OnEnemyHealthChanged != null)
			BattleEvents.OnEnemyHealthChanged(fighterData.Stats.HealthPoints);
	}

	// show line of vision gizmo
	// TODO: testing / debugging only!
	//private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawRay(transform.position, transform.forward * maxVisionDist);
    //}
}
